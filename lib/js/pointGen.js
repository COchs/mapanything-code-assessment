"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* transpiled using babel 6.26.0*/

/*getRandomIntInclusive retrieved from MDN web docs at https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random*/
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
  /*The maximum is inclusive and the minimum is inclusive*/
}
/* class object to generate a marker for geospatial coordinates randomly on the globe*/


var point =
/*#__PURE__*/
function () {
  function point() {
    _classCallCheck(this, point);

    /*generates random lat and lon for object*/
    this.lat = getRandomIntInclusive(-90, 90);
    this.lon = getRandomIntInclusive(-180, 180);
  }
  /*returns lat and lon in coordinate format readable by leaflet*/


  _createClass(point, [{
    key: "quadrant",

    /*determines the quadrant string based on the lat and lon and returns it in camelcase*/
    value: function quadrant() {
      var quadrant = '';

      if (this.lat < 0) {
        quadrant = 'south';
      } else {
        quadrant = 'north';
      }

      if (this.lon < 0) {
        quadrant += 'West';
      } else {
        quadrant += 'East';
      }

      return quadrant;
    }
  }, {
    key: "coord",
    get: function get() {
      return [this.lat, this.lon];
    }
    /*returns the quadrant on the globe the point can be found in for layer and icon assignment*/

  }, {
    key: "quad",
    get: function get() {
      return this.quadrant();
    }
  }]);

  return point;
}();