/* transpiled using babel 6.26.0*/

window.onload=function(){
  /*declare leaflet map*/
  const map = L.map('map', {
    center: [0,0],
    zoom: 3,
    worldCopyJump: true,
    minZoom: 3
  });

  /*declare individual icon html for the appropriate layers*/
  const northEastIcon = L.divIcon({
    className: 'NorthEastIcon',
    html: `<i class="fas fa-map-marker-alt fa-lg NEIcon"></i>`
  });
  const northWestIcon = L.divIcon({
    className: 'NorthWestIcon',
    html: `<i class="fas fa-map-marker-alt fa-lg NWIcon"></i>`
  });
  const southEastIcon = L.divIcon({
    className: 'SouthEastIcon',
    html: `<i class="fas fa-map-marker-alt fa-lg SEIcon"></i>`
  });
  const southWestIcon = L.divIcon({
    className: 'SouthWestIcon',
    html: `<i class="fas fa-map-marker-alt fa-lg SWIcon"></i>`
  });

  /*declare leaflet layers*/
  const northEastLayer = L.layerGroup([]).addTo(map);
  const northWestLayer = L.layerGroup([]).addTo(map);
  const southEastLayer = L.layerGroup([]).addTo(map);
  const southWestLayer = L.layerGroup([]).addTo(map);

  /*build the icon association for layers in the legend*/
  let overlayMarkers = {
    '<i class="fas fa-map-marker-alt NEIcon"></i><span class="layerLabel">Northeast</span><span class="NECount"></span>': northEastLayer,
    '<i class="fas fa-map-marker-alt NWIcon"></i><span class="layerLabel">Northwest</span><span class="NWCount"></span>': northWestLayer,
    '<i class="fas fa-map-marker-alt SEIcon"></i><span class="layerLabel">Southeast</span><span class="SECount"></span>': southEastLayer,
    '<i class="fas fa-map-marker-alt SWIcon"></i><span class="layerLabel">Southwest</span><span class="SWCount"></span>': southWestLayer
  }

  /*build the abstracted object for marker generation and layer assignment*/
  let quadrantObject = {
    northEast:{
      layer: northEastLayer,
      icon: northEastIcon,
      countClass: "NECount"
    },
    northWest:{
      layer:  northWestLayer,
      icon: northWestIcon,
      countClass: "NWCount"
    },
    southEast:{
      layer: southEastLayer,
      icon: southEastIcon,
      countClass: "SECount"
    },
    southWest:{
      layer: southWestLayer,
      icon: southWestIcon,
      countClass: "SWCount"
    }
  };

  /*add OSM tile layer for a visible map of globe*/
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  /*build Leaflet control for the refresh button*/
  let refreshControl = L.Control.extend({
    onAdd: function(map){
      /*declare the refresh button DOM association assign type, text, and click handler*/
      const refreshButton = L.DomUtil.create('input', 'markerRefresh');
      refreshButton.type = "button";
      refreshButton.value = "Refresh";
      refreshButton.onclick = function(){
        /*call the function to clear layers, generate new markers, and add marker counts to the legend*/
        clearAllLayers(quadrantObject);
        refreshMarkers(quadrantObject);
        applyLayerCounts(quadrantObject);
      }

      return refreshButton;
    }
  });

  /*add refresh button to map*/
  map.addControl(new refreshControl());

  /*add layers containing markers to map, based on previous association*/
  L.control.layers(null, overlayMarkers).addTo(map);

  /*initial run of marker creation and addition to layers/legend*/
  refreshMarkers(quadrantObject);
  applyLayerCounts(quadrantObject);

};

/*clears all layers; expects an object matching a minimum of the following {layerName:{layer: leafletLayerObject}}*/
function clearAllLayers (quads){
  for(let key of Object.keys(quads)){
    /*clear each layer in order of the association in the prebuilt quadrant object*/
    quads[key].layer.clearLayers();
  }
}

/*takes layers and determines the marker count in the quadrant group for display in the legend; expects an object matching a minimum of the following {layerName:{layer: leafletLayerObject, countClass: HTMLclassString}}*/
function applyLayerCounts (quads){
  for(let key of Object.keys(quads)){
    /*determine count in each layer and return it to the legend*/
    document.getElementsByClassName(quads[key].countClass)[0].innerHTML = quads[key].layer.getLayers().length;
  }
}

/*creates 2000 random points based off pointGen.js; expects an object matching a minimum of the following {layerName:{layer: layerObject, icon: leafletIconObject}}*/
function refreshMarkers (quads){
  /*create local array to house the generated markers for the map*/
  let markers = [];

  /*create 2000 instances of the point object for markers; This could easily be adjusted to accept user input for the number of desired points*/
  for(let i = 0; i < 2000; i++){
    /*add new point to the markers array*/
    markers.push( new point());
  }

  /*add the appropriate icon and layer to each marker based on the quadrant it was randomly generated in*/
  for(let marker of markers){
    let markericon = quads[marker.quad].icon;
    let layerGroup = quads[marker.quad].layer;

    /*check for a null object in the markericon and apply the point to the map for error visibility*/
    if (markericon != null){
      L.marker(marker.coord, {icon: markericon}).addTo(layerGroup);
    } else {
      L.marker(marker.coord).addTo(map);
    }
  }
}
