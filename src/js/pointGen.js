/* transpiled using babel 6.26.0*/

/*getRandomIntInclusive retrieved from MDN web docs at https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random*/
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; /*The maximum is inclusive and the minimum is inclusive*/
}

/* class object to generate a marker for geospatial coordinates randomly on the globe*/
class point {
  constructor() {
    /*generates random lat and lon for object*/
    this.lat = getRandomIntInclusive(-90,90);
    this.lon = getRandomIntInclusive(-180,180);
  }

  /*returns lat and lon in coordinate format readable by leaflet*/
  get coord(){
    return [this.lat, this.lon];
  }

  /*returns the quadrant on the globe the point can be found in for layer and icon assignment*/
  get quad(){
    return this.quadrant();
  }

  /*determines the quadrant string based on the lat and lon and returns it in camelcase*/
  quadrant (){
    let quadrant = '';
    if (this.lat < 0){
      quadrant = 'south';
    } else {
      quadrant = 'north';
    }
    if (this.lon < 0){
      quadrant += 'West';
    } else {
      quadrant += 'East';
    }
    return quadrant;
  }
}
